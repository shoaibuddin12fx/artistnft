package com.artistnft;

import android.content.Intent;
import android.provider.Settings;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.thrivecom.ringcaptcha.RingcaptchaApplication;
import com.thrivecom.ringcaptcha.RingcaptchaApplicationHandler;
import com.thrivecom.ringcaptcha.RingcaptchaVerification;
import com.thrivecom.ringcaptcha.SetNumberActivity;

public class RcOtp extends ReactContextBaseJavaModule {
    private static ReactApplicationContext reactContext;

    RcOtp(ReactApplicationContext context) {
        super(context);
        reactContext = context;
    }

    @ReactMethod
    public void show() {
        Toast.makeText(reactContext, "Hi from Android", Toast.LENGTH_LONG).show();
    }

    @ReactMethod
    public void verifyPhoneNumber(Promise promise) {
        ReactApplicationContext context = getReactApplicationContext();
        Intent intent = new Intent(context, SetNumberActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);

        try {
//
//            RingcaptchaApplication.verifyPhoneNumber(getReactApplicationContext(),"uta9u6i7ozybyqoga9ac", "a2ane1i9ohi4u9o3e3uf", new RingcaptchaApplicationHandler() {
//                @Override
//                public void onSuccess(RingcaptchaVerification ringcaptchaVerification) {
//                    //Verification successful
                    promise.resolve("verified");
//                }
//                @Override
//                public void onCancel() {
//                    //Decide what to do if user cancelled operation
//                    promise.resolve("not-verified");
//                }
//            });
        } catch (Exception e) {
            promise.reject("Error", e);
        }
    }

    @NonNull
    @Override
    public String getName() {
        return "RcOtp";
    }
}