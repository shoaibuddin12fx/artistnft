import React, { useState, useEffect } from 'react'
import { View, Text } from 'react-native'
import styles from './styles'

const languages = ["English", "French", "Spanish", "Chinese", "Hindi"]

export default function LanguageSelector({ toggler }) {
    const [isPressed, setIsPressed] = useState(false)

    useEffect(() => {
        setIsPressed(false)
    }, [toggler])

    return (
        <View style={styles.container}>
            <Text style={styles.text} onPress={()=>setIsPressed(prev => !prev)}>Choose Language</Text>

            {
                isPressed ?
                    <View style={styles.dropDown}>
                        {
                            languages.map(lang => (
                                <Text style={styles.item}>{lang}</Text>
                            ))
                        }
                    </View>
                :
                    null
            }
        </View>
    )
}
