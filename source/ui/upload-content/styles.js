import {Dimensions, StyleSheet} from 'react-native';
import theme from 'source/constants/colors';

const {height, width} = Dimensions.get('window');

export default StyleSheet.create({
  scrollViewContainer: {
    height: '120%',
  },
  ViewHeight: {
    height: '140%',
  },
  eventViewHeight: {
    height: '155%',
  },
  EventView: {
    height: 180,
    width: '90%',
    borderRadius: 5,
    // flexDirection: 'row',
    alignItems: 'center',
    // backgroundColor: 'rgba(0, 0, 0, 0.5)',

    // paddingLeft: 11,
    // paddingRight: 11,
    elevation: 3,

    shadowColor: 'rgba(255, 255, 255, 0.2)',
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.2,
    shadowRadius: 2,
  },
  container: {
    flex: 1,
    height: '100%',
    alignItems: 'center',
    backgroundColor: theme.primary,
  },
  appName: {
    height: 28,
    width: 174.27,
    marginTop: 18,
    resizeMode: 'contain',
  },
  imageSize: {
    height: 60,
    width: 60,
    borderRadius: 30,
    // marginTop: 18,
    // resizeMode: 'contain',
  },
  image: {
    marginTop: 50,
    height: height / 3.5,
    resizeMode: 'contain',
  },
  heading1: {
    fontSize: 15,
    marginTop: 30,
    color: '#C6C6C6',
    textAlign: 'center',
    fontFamily: 'Sansation_Bold',
  },
  heading2: {
    fontSize: 15,
    marginTop: 22,
    color: '#C6C6C6',
    marginBottom: 45,
    textAlign: 'center',
    fontFamily: 'Sansation_Bold',
  },
  inputSeach: {
    marginTop: 30,
  },
  input: {
    marginTop: 5,
  },
  uploadIconsView: {
    height: 155,
    width: '85%',
    borderRadius: 10,
    backgroundColor: '#474747',
    alignItems: 'center',
    paddingVertical: '5%',
    paddingHorizontal: '12%',
    marginTop: 20,
  },
  uploadIcon: {
    height: 49.69,
    width: 49.69,
    resizeMode: 'contain',
  },
  checkImageSize: {
    height: 20,
    width: 20,
    resizeMode: 'contain',
  },
  uploadIconsBg: {
    backgroundColor: 'white',
    height: 41.69,
    width: 41.69,
    borderRadius: 20.845,
  },
  flexDirection: {
    flexDirection: 'row',
  },
  marginTop: {
    marginTop: 20,
  },
  ImageBackground: {
    backgroundColor: '#474747',
    height: 44,
    width: 44,
    borderRadius: 3,
    alignItems: 'center',
    justifyContent: 'center',
  },
  ImageBackground2: {
    backgroundColor: '#474747',
    height: 100,
    width: 100,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  photoIcon: {
    height: 25,
    width: 35,
    resizeMode: 'contain',
  },
  photoIcon2: {
    height: 70,
    width: 70,
    resizeMode: 'contain',
  },
  rowViewStyle: {
    width: 335,
    height: 52,
    // backgroundColor: 'red',
    borderRadius: 5,
  },
  youtubeIcon: {
    height: 30,
    width: 30,
    resizeMode: 'contain',
  },
  ViewShadow: {
    height: 59,
    width: '90%',
    borderRadius: 5,
    flexDirection: 'row',
    alignItems: 'center',
    // paddingLeft: 11,
    // paddingRight: 11,
    elevation: 3,

    shadowColor: 'rgba(255, 255, 255, 0.2)',
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.2,
    shadowRadius: 2,
  },
  rowShadowView: {
    height: 59,
    width: '45%',
    borderRadius: 5,
    flexDirection: 'row',
    alignItems: 'center',
    // paddingLeft: 11,
    // paddingRight: 11,
    elevation: 3,

    shadowColor: 'rgba(255, 255, 255, 0.2)',
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.2,
    shadowRadius: 2,
  },
  justifyContentSpace: {
    justifyContent: 'space-between',
  },
  alignSelf: {
    alignSelf: 'center',
  },
  closeIcon: {
    height: 13,
    width: 13,
    resizeMode: 'contain',
    marginTop: 5,
    marginLeft: 60,
  },
  ApproveBtn: {
    height: 52,
    width: 287,
    backgroundColor: '#EA4836',
    borderRadius: 10,
    marginTop: 80,
    alignItems: 'center',
    justifyContent: 'center',
  },
  NextBtn: {
    height: 52,
    width: 315,
    backgroundColor: '#EA4836',
    borderRadius: 10,
    marginTop: 20,
    marginBottom: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  TicketBtn: {
    height: 52,
    width: 280,
    backgroundColor: '#EA4836',
    borderRadius: 10,
    marginBottom: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  modalView: {
    backgroundColor: 'green',
  },
  modalText: {
    fontSize: 15,
    marginTop: 20,
    color: '#EA4836',
    textAlign: 'center',
    fontFamily: 'Sansation_Bold',
  },
});
