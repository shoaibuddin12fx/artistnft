import React from 'react';
import { View, Image, Text, ScrollView } from 'react-native';
import styles from './styles';

import APP_NAME from 'source/assets/app_name.png';
import Button from 'source/common/button';
import OTP from 'source/assets/otp.png';
import VERIFIED from 'source/assets/verified.png';

export default function Selection() {
    return (
        <ScrollView contentContainerStyle={styles.scrollview}>
            <View style={styles.container}>
                
                <Image
                    source={APP_NAME}
                    style={styles.appName}
                />

                <Text style={[styles.heading1, { marginTop: 35 }]}>Phone Verification</Text>
                
                <View>
                    <Image
                        source={OTP}
                        style={styles.otpImage}
                    />
                    <Image
                        source={VERIFIED}
                        style={styles.verifiedImage}
                    />
                </View>

                <Text style={[styles.heading2, { fontSize: 18 }]}>
                    Verified
                </Text>

                <View style={{ marginTop: 'auto', width: '100%', paddingBottom: 15 }}>
                    <Button
                        title="NEXT"
                        onPress={()=>alert('Yes')}
                        style={{ marginLeft: 'auto', marginRight: 'auto' }}
                    />
                </View>
            </View>
        </ScrollView>
    )
}