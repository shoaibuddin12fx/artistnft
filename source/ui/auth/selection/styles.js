import { Dimensions, StyleSheet } from 'react-native';
import theme from 'source/constants/colors';

const { height, width } = Dimensions.get('window');

export default StyleSheet.create({
    scrollViewContainer: {
        height: "100%"
    },
    container: {
        flex: 1,
        height: "100%",
        alignItems: 'center',
        backgroundColor: theme.primary
    },
    appName: {
        height: 22,
        marginTop: 18,
        resizeMode: 'contain',
    },
    image: {
        marginTop: 50,
        height: height /3.5,
        resizeMode: 'contain',
    },
    heading1: {
        fontSize: 20,
        marginTop: 50,
        color: 'white',
        textAlign: 'center',
        fontFamily: 'Sansation_Bold'
    },
    heading2: {
        fontSize: 18,
        marginTop: 22,
        color: '#C6C6C6',
        marginBottom: 45,
        textAlign: 'center',
        fontFamily: 'Sansation_Bold'
    }
})